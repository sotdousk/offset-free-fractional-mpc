import numpy as np
from math import factorial
from fMPC import my_f_mpc
from control import obsv, ctrb
import matplotlib.pyplot as plt
from kalman import KalmanFilterLinear
from datetime import datetime
__author__ = 'douskas'


def is_strictly_schur(x):
    return np.all(np.linalg.norm(np.linalg.eigvals(x) < 1))


def check_obs_cont(a_m, b_m, c_m):
    print "Checking for Observability and Controllability..."
    obs = np.linalg.matrix_rank(obsv(a_m, c_m)) == a_m.shape[0]
    ctr = np.linalg.matrix_rank(ctrb(a_m, b_m)) == a_m.shape[0]
    if obs and ctr:
        return "System is both Observable and Controllable."
    elif obs and not ctr:
        return "System is Observable but Not Controllable."
    elif not obs and ctr:
        return "System is Not Observable but it is Controllable."
    else:
        return "System is Not Observable Nor Controllable."


def cj(derivative, j):
    """

    :rtype : float
    """
    if j == 0:
        return 1
    else:
        prod = derivative
        end = j-1
        for i in range(1, end+1):
            prod *= derivative - i
        return (-1)**j * prod / factorial(j)

a = 0.587
k10 = 1.4913
k12 = 2.9522
k21 = 0.4854
k0 = k12 + k10

b = 1 - a
step = 0.1
end_sim = 10.
memory_length = 25
mpc_horizon = 10  # This variable is used only for the fractional MPC - Suggested Value N = 60

M = np.array([[-k0, 0.], [k21, 0.]])
Theta = np.array([[0., k21], [0., -k21]])
Bs = np.array([[1.], [0.]])
I = np.eye(2)
Lambda = M * step + I + Theta * step**a
B_sot = np.array([[1.], [0.]])


def a_matrix(order):
    am1 = Lambda
    for i in range(order-1):
        am1 = np.concatenate((am1, Theta*(step**a)*cj(b, i)), axis=1)
    am2 = np.eye(2*(order-1))
    am3 = np.zeros((2*(order-1), 2), dtype="float")
    am23 = np.bmat([[am2, am3]])
    am = np.bmat([[am1], [am23]])
    return am


def b_matrix(order):
    bm = np.zeros((2*order, 1), dtype="float")
    bm[0: 2] = Bs * step
    return bm


def c_matrix(order):
    cm = np.zeros((1, 2*order))
    cm[0, 0] = 1
    return cm


def frac_sys(order):
    af = a_matrix(order)
    bf = b_matrix(order)
    cf = c_matrix(order)
    # print check_obs_cont(af, bf, cf)
    return af, bf, cf

A, B, C = frac_sys(memory_length)
Va = np.array([[1./(k10*k21), 1.]])
Vna = np.array([[a, 0.]])
Vb = np.array([[1./(k10*k21), 1./k10, (k10+k12)/(k10*k21), 1.]])
Vnb = np.array([[a+1, 1, a, 0.]])


def frac_step_sim(t):
    length = t.shape[0]
    x_real = np.zeros((A.shape[0], length))
    x_k_real = x_real[:, 0:1]
    x_k_predicted = x_real[:, 0:1]

    response_real = np.zeros((length, 1))
    state_x2 = np.zeros((length, 1))
    inp_array = np.zeros((length, 1))

    if is_strictly_schur(A):
        print "A is strictly Schur."
    else:
        print "A is not strictly Schur."

    bd = np.zeros((A.shape[0], B.shape[1]))
    cd = np.ones((C.shape[0], B.shape[1]))

    # KALMAN MATRICES
    a_kalman = np.bmat([[A, bd], [np.zeros((C.shape[0], A.shape[1])), np.eye(bd.shape[1])]])
    b_kalman = np.bmat([[B], [np.zeros((B.shape[1], B.shape[1]))]])
    c_kalman = np.bmat([[C, cd]])

    p_kalman = 1. * np.eye(a_kalman.shape[0])
    q_kalman = 1. * np.eye(a_kalman.shape[0])
    r_kalman = 1.e-1 * np.eye(1)

    # Set Target
    target = 1. * np.ones((1, 1))

    # Set Initial Values
    d_p = np.array([[0.01]])
    inp = np.array([[1.]])
    j_k_predicted = np.concatenate((x_k_predicted, d_p), axis=0)
    # j_k_predicted += 0.5 * np.ones((j_k_predicted.shape[0], j_k_predicted.shape[1]))
    kalman = KalmanFilterLinear(a_kalman, b_kalman, c_kalman, j_k_predicted, p_kalman, q_kalman, r_kalman)
    start = datetime.now()
    for i in range(length+1):
        print "Running " + str(i) + ", " + str(length+1 - i) + " left."

        # REAL SYSTEM - NO NOISE, NO ERRORS
        x_new = np.dot(A, x_k_real) + np.dot(B, inp)
        y_real = np.dot(C, x_k_real)

        response_real[i: i+1] = y_real

        # KALMAN PREDICTION
        x_predicted = j_k_predicted[0:A.shape[0], :]
        d_p = j_k_predicted[A.shape[0]::, :]
        kalman.step(inp, y_real)
        j_k_predicted = kalman.getCurrentState()
        # if j_k_predicted[1, 0] < 0:               # This is because state x2 gets negative at the beginning
        #     j_k_predicted[1, 0] = 0
        state_x2[i: i+1] = j_k_predicted[1, 0]

        # MPC Controller
        x_predicted[0, 0] = y_real[0, 0]
        # x_predicted[2:A.shape[0], :] = x_k_real[2::, :]
        inp = my_f_mpc(mpc_horizon, target, A, B, C, bd, cd, x_predicted, d_p)
        inp_array[i: i+1] = inp
        x_k_real = x_new
        print y_real

    print "Total running time: " + str(datetime.now() - start)
    print "Accuracy: " + str(min(y_real, target)[0, 0] * 100. / max(y_real, target)[0, 0])\
          + "% of the desired value."
    # print "y max value = " + str(max(response_predicted))
    return response_real, state_x2, inp_array

time = np.arange(0, end_sim-step, step)
yr, x2p, u = frac_step_sim(time)
plt.figure(1)
plt.plot(time, x2p, 'b')
plt.plot(time, yr, 'r-')
plt.figure(2)
plt.step(time, u)
# plt.figure(3)
# plt.plot(yr, x2p)
plt.show()