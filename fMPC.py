__author__ = 'douskas'
from cvxopt import solvers, matrix
from scipy.linalg import eigh
import numpy as np
from scipy.linalg import block_diag
from yottalab import bb_dare


def is_psd(a, tol=1e-8):
    """
    Check is a matrix is positive semi-definite
    :param a:
    :param tol:
    :return: logical statement (True or False)
    """
    e = eigh(a)[0]
    return np.all(e > -tol)


def ss_opt(a, b, c, bd, cd, ref, d_hat):
    mq = a.shape[0]
    mr = b.shape[1]
    a_mpc = np.bmat([[a - np.eye(mq), b], [c, np.zeros((mr, mr))]])
    b_mpc = np.bmat([[-np.dot(bd, d_hat)], [ref - np.dot(cd, d_hat)]])
    x_mpc = np.linalg.solve(a_mpc, b_mpc)
    x_ = x_mpc[0:mq, :]
    u_ = x_mpc[mq::, :]
    return x_, u_


def my_f_mpc(horizon, ref, a, b, c, bd, cd, x_hat, d_hat):
    mq = a.shape[1]
    mr = b.shape[1]
    q = 1.e1 * np.eye(mq, dtype="float")

    r = 5. * np.eye(mr, dtype="float")
    qf = bb_dare(a, b, q, r)
    # qf = q
    x_s, u_s = ss_opt(a, b, c, bd, cd, ref, d_hat)
    '''
    V = 1/2 * z' * H * z + q_lin * z --> Cost function
    '''
    # ----- H matrix ----- #
    hc = np.zeros((horizon * (mq + mr), horizon * (mq + mr)), dtype="float")
    for k in range(0, horizon*(mq+mr), (mq+mr)):
        hc[k*mr: (k+1)*mr, k*mr: (k+1)*mr] = r
        hc[(k+1)*mr: (k+1)*mr + mq, (k+1)*mr: (k+1)*mr + mq] = q
        if k == (horizon-1)*(mq+mr):
            hc[(k+1)*mr: (k+1)*mr + mq, (k+1)*mr: (k+1)*mr + mq] = qf
    hc *= 2.
    # -------------------- #

    # --- q_lin matrix --- #
    q_lin = np.zeros((horizon*(mq+mr), mr), dtype="float")
    for k in range(0, (horizon-1)*(mq+mr), (mq+mr)):
        q_lin[k*mr: (k+1)*mr, :] = np.dot(u_s, r).T
        q_lin[(k+1)*mr: (k+1)*mr + mq, :] = np.dot(x_s.T, q).T
    q_lin[(horizon-1)*(mq+mr): (horizon-1)*(mr+mq)+mr, :] = np.dot(u_s, r)
    q_lin[(horizon-1)*(mr+mq)+mr::, :] = np.dot(x_s.T, qf).T
    q_lin *= -2.
    # -------------------- #
    '''
    G * q < h --> Inequality constraints
    '''
    # ----- G matrix ----- #
    eye_r = np.bmat([[-np.eye(mr), np.zeros((mr, mq))], [np.eye(mr), np.zeros((mr, mq))]])
    eye_q = np.bmat([[np.zeros((mq, mr)), -np.eye(mq)], [np.zeros((mq, mr)), np.eye(mq)]])
    eye_diag = np.bmat([[eye_r], [eye_q]])
    g = np.bmat([[eye_r], [eye_q]])
    for i in range(horizon-1):
        g = block_diag(g, eye_diag)
    # -------------------- #

    # ----- h matrix ----- #
    x_low = -0.1
    x1_up = 2.5
    x2_up = 1.0
    u_low = -0.
    u_up = 9.5
    hm = np.array([[-u_low], [u_up]])
    for i in range(mq):
        hm = np.concatenate((hm, np.array([[-x_low]])), axis=0)
    for i in range(mq/2):
        hm = np.concatenate((hm, np.array([[x1_up], [x2_up]])))
    h = hm
    for i in range(1, horizon):
        h = np.concatenate((h, hm), axis=0)
    # -------------------- #
    '''
    x_(k+1) = A * x_(k) + B * u_(k) --> Equality constraints
    '''
    # ----- A matrix ----- #
    a_til = np.zeros((horizon * mq, horizon * (mr + mq)), dtype="float")
    first_row_atil = np.bmat([-b, np.eye(mq, dtype="float")],)
    second_row_atil = np.bmat([-a, -b, np.eye(mq, dtype="float")])
    a_til[0:b.shape[0], 0:(mq + mr)] = first_row_atil
    for k in range(1, horizon):
        a_til[k*b.shape[0]:(k+1)*b.shape[0],
              k*mr+(k-1)*mq:(k+1)*mr+(k+1)*mq] = second_row_atil
    # -------------------- #

    # ----- B matrix ----- #
    b_til = np.zeros((horizon*mq, 1), dtype="float")
    b_til[0:mq, 0: mr] = np.dot(bd, d_hat) + np.dot(a, x_hat)
    for k in range(1, horizon):
        b_til[k*b.shape[0]: (k+1)*b.shape[0]] = np.dot(bd, d_hat)
    # -------------------- #

    # solvers.options['abstol'] = 1.e-324
    sol = solvers.qp(matrix(hc), matrix(q_lin), matrix(g), matrix(h),
                     matrix(a_til), matrix(b_til))

    if sol['x'][0] <= 0:
        u = 0
    else:
        u = sol['x'][0]

    if sol['status'] == 'optimal':
        return u
    else:
        return "Didn't found an optimal solution."