import numpy as np
from numpy.linalg import inv
# Implements a linear Kalman filter.


class KalmanFilterLinear:
    def __init__(self, _a, _b, _c, _x, _p, _q, _r):
        self.A = _a                         # State transition matrix.
        self.B = _b                         # Control matrix.
        self.H = _c                         # Observation matrix.
        self.current_state_estimate = _x    # Initial state estimate.
        self.current_prob_estimate = _p     # Initial covariance estimate.
        self.Q = _q                         # Estimated error in process.
        self.R = _r                         # Estimated error in measurements.

    def getCurrentState(self):
        return self.current_state_estimate

    def step(self, control_vector, measurement_vector):
        # ---------------------------Prediction step-----------------------------
        predicted_state_estimate = np.dot(self.A, self.current_state_estimate) + np.dot(self.B, control_vector)
        predicted_prob_estimate = np.dot(self.A, np.dot(self.current_prob_estimate, np.transpose(self.A))) + self.Q
        # --------------------------Observation step-----------------------------
        innovation = measurement_vector - np.dot(self.H, predicted_state_estimate)
        innovation_covariance = np.dot(self.H, np.dot(predicted_prob_estimate, np.transpose(self.H))) + self.R
        # -----------------------------Update step-------------------------------
        kalman_gain = np.dot(predicted_prob_estimate,
                             np.dot(np.transpose(self.H), inv(innovation_covariance)))
        self.current_state_estimate = predicted_state_estimate + np.dot(kalman_gain, innovation)
        # We need the size of the matrix so we can make an identity matrix.
        size = self.current_prob_estimate.shape[0]
        # eye(n) = nxn identity matrix.
        self.current_prob_estimate = np.dot((np.eye(size) - np.dot(kalman_gain, self.H)), predicted_prob_estimate)