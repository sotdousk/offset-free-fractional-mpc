# Fractional Offset Free MPC

**Offset Free Model Predictive Control of Fractional-Order
Pharmacokinetic Models - Simulation Framework.**



### Instructions

1. Download and install [Python](https://www.python.org) - preferred version 2.7 
2. Download and install [cvxopt](http://cvxopt.org) - solver for Python optimization
3. Download and install [numpy scipy matplotlib](hhttp://www.scipy.org/install.html) - 
	Libraries for scientific computing in Python 
4. Download and install [control](http://sourceforge.net/p/python-control/wiki/Download/) - 
	 Python package that implements basic operations for analysis and design of feedback control systems.
5. Download and install [slycot](https://github.com/repagh/Slycot) - 
	Python wrapper for selected SLICOT routines
6. Download and install [yottalab](https://github.com/goretkin/kinodyn/blob/master/yottalab.py) in 
	your working project folder.
7. Run matricesFormulation.py to plot the results.

*Remark: _You are encouraged to download and install [pyCharm](https://www.jetbrains.com/pycharm/)
	an IDE for Python as this project is written in pyCharm._


### Files

* __matricesFormulationTest.py__: Initialize fractional order system, approximates in finite dimensions, 
simulates MPC response and compares the the real one.
* __fMPC.py__: Designs and solves the MPC problem.
* __trueFractional.py__: Simulates the real Fractional Order System.
* __test.py__: Some useful functions.
* __yottalab.py__: Very useful Functions for Control Systems.