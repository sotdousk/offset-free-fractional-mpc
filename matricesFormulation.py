import numpy as np
from math import factorial
from fMPC import my_f_mpc
from control import obsv, ctrb
import matplotlib.pyplot as plt
__author__ = 'douskas'


def is_strictly_schur(x):
    return np.all(np.linalg.norm(np.linalg.eigvals(x) < 1))


def check_obs_cont(a_m, b_m, c_m):
    print "Checking for Observability and Controllability..."
    obs = np.linalg.matrix_rank(obsv(a_m, c_m)) == a_m.shape[0]
    ctr = np.linalg.matrix_rank(ctrb(a_m, b_m)) == a_m.shape[0]
    if obs and ctr:
        return "System is both Observable and Controllable."
    elif obs and not ctr:
        return "System is Observable but Not Controllable."
    elif not obs and ctr:
        return "System is Not Observable but it is Controllable."
    else:
        return "System is Not Observable Nor Controllable."


def cj(derivative, j):
    """

    :rtype : float
    """
    if j == 0:
        return 1
    else:
        prod = derivative
        end = j-1
        for i in range(1, end+1):
            prod *= derivative - i
        return (-1)**j * prod / factorial(j)

a = 0.587
k10 = 1.4913
k12 = 2.9522
k21 = 0.4854
k0 = k12 + k10

b = 1 - a
step = 0.1
end_sim = 5.
memory_length = 3
mpc_horizon = 3  # This variable is used only for the fractional MPC

M = np.array([[-k0, 0.], [k21, 0.]])
Theta = np.array([[0., k21], [0., -k21]])
Bs = np.array([[1.], [0.]])
I = np.eye(2)
Lambda = M * step + I + Theta * step**a
B_sot = np.array([[1.], [0.]])


def true_simulation(my_time, u):
    length = len(my_time)
    x_all = np.zeros((2, length))
    for i in range(1, length):
        total = np.dot(Lambda, x_all[:, i-1: i]) + np.dot(B_sot, step * u[i: i+1, 0:1])
        for j in range(1, i+1):
            total += np.dot(Theta * cj(b, j) * step ** a, x_all[:, j-1: j])
        x_all[:, i: i+1] = total
    return x_all


def a_matrix(order):
    am1 = Lambda
    for i in range(order):
        am1 = np.concatenate((am1, Theta*(step**a)*cj(b, i)), axis=1)
    am2 = np.eye(2*order)
    am3 = np.zeros((2*order, 2), dtype="float")
    am23 = np.bmat([[am2, am3]])
    am = np.bmat([[am1], [am23]])
    return am


def b_matrix(order):
    bm = np.zeros((2*(order+1), 1), dtype="float")
    bm[0: 2] = Bs * step
    return bm


def c_matrix(order):
    cm = np.zeros((1, 2*(order+1)))
    cm[0, 0] = 1
    return cm


def frac_sys(order):
    af = a_matrix(order)
    bf = b_matrix(order)
    cf = c_matrix(order)
    print check_obs_cont(af, bf, cf)
    return af, bf, cf

A, B, C = frac_sys(memory_length)


def frac_step_sim(t):
    length = t.shape[0]
    x = np.zeros((A.shape[0], length))
    x_k = x[:, 0: 1]
    u_matrix = np.zeros((length, 1))
    response = np.zeros((length, 1))
    dist = np.zeros((length, 1))

    if is_strictly_schur(A):
        print "A is strictly Schur."
        bd = np.zeros((A.shape[0], B.shape[1]))
        cd = np.ones((C.shape[0], B.shape[1]))
        lx = np.zeros((A.shape[0], B.shape[1]))
        ly = np.ones((C.shape[0], B.shape[1]))
        l = np.concatenate((lx, ly), axis=0)
    else:
        print "A is not strictly Schur."

    # Kalman matrices
    a_kalman = np.bmat([[A, bd],
                        [np.zeros((B.shape[1], A.shape[1])), np.eye(B.shape[1])]])
    b_kalman = np.bmat([[B], [np.zeros((B.shape[1], B.shape[1]))]])
    c_kalman = np.zeros((1, 2*(memory_length+1)+1))
    c_kalman[0, 0] = 1
    c_kalman = np.bmat([[c_kalman], [np.zeros((2*memory_length+1, 2)), np.eye(2*memory_length+1)]])

    # pk = np.eye(a_kalman.shape[0])
    # qk = np.zeros((a_kalman.shape[0], a_kalman.shape[1]))
    # # qk = np.eye(a_kalman.shape[0])
    # rk = np.array([[0.01]])
    print "Check for Kalman observability..."
    print check_obs_cont(a_kalman, b_kalman, c_kalman)
    #

    target = np.array([[1.]])

    # Initial estimations for the observer
    d_p = np.array([[5.e-1]])
    inp = np.array([[0.]])
    ym = np.dot(C, x_k)
    for i in range(1, length+1):
        print "Running " + str(i) + ", " + str(length+1 - i) + " left."
        if i >= 25:
            target = np.array([[1.5]])
        x_pred = np.dot(A, x_k) + np.dot(B, inp) + np.dot(bd, d_p)
        d_pred = d_p
        dist[i: i+1] = d_p
        j_pred = np.concatenate((x_pred, d_pred), axis=0)
        # ---------------------- ************* ---------------------- #
        # Here comes the Estimator
        check_matrix1 = np.bmat([[A, bd], [np.zeros((C.shape[0], A.shape[1])), np.eye(C.shape[0])]])
        check_matrix2 = np.dot(np.bmat([[lx], [ly]]), np.bmat([[C, cd]]))
        check_matrix = check_matrix1 - np.dot(check_matrix1, check_matrix2)
        if is_strictly_schur(check_matrix):
            print "Estimator is Stable"
        else:
            return "Unstable Estimator"
        par = ym - np.dot(np.bmat([[C, cd]]), j_pred)
        j_k = j_pred + np.dot(l, par)
        x_k = j_k[0: A.shape[0], :]
        d_p = j_k[A.shape[0]::, :]
        # ---------------------- ************* ---------------------- #

        # MPC Controller
        inp = my_f_mpc(mpc_horizon, target, A, B, C, bd, cd, x_k, d_p)

        # Fill the corresponding matrices
        ym = np.dot(C, x_k)
        response[i: i+1] = ym
        u_matrix[i: i+1] = inp
        print ym

    return response, u_matrix, dist

time = np.arange(0, end_sim-step, step)
yr, u_list, disturbance = frac_step_sim(time)

plt.figure(1)
# plt.subplot(211)
plt.title("Response of fractional-order pharmacokinetic model")
plt.ylim([0, 1.20001*max(yr)])
plt.step(time, yr, 'r-')
plt.xlabel("time (h)")
plt.ylabel("$A_1$ amount in plasma (ng)")
ax = plt.axes()
ax.yaxis.grid()  # vertical lines
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# plt.subplot(212)
plt.figure(2)
plt.title("Administration Rate")
plt.step(time, u_list, 'b')
plt.xlabel("time (h)")
plt.ylabel("Input - i.v. (ng/hr)")
ax = plt.axes()
ax.yaxis.grid()  # vertical lines
# Simulate the real system...
# plt.figure(3)
y_real = true_simulation(time, u_list)[0:1, :]
dif = yr - y_real.T     # Difference between simulated and real fractional system.
plt.figure(3)
# plt.plot(time, y_real[0], 'g')  # , time, dif, 'b')
# plt.step(time, yr, 'r')
plt.plot(time, y_real[0], '-g', label='Real System')
plt.step(time, yr, '-r', label='Approximated System')
plt.legend(loc='lower right')
plt.title("Responses of the Real and of the Approximated Fractional-order Systems")
plt.xlabel("time (hr)")
plt.grid()
print dif
plt.show()