__author__ = 'douskas'

from matricesFormulation import *


def cj(derivative, j):
    """

    :rtype : float
    """
    if j == 0:
        return 1
    else:
        prod = derivative
        end = j-1
        for i in range(1, end+1):
            prod *= derivative - i
        return (-1)**j * prod / factorial(j)


def true_simulation(my_time, u):
    length = len(my_time)
    x_all = np.zeros((2, length))
    for i in range(1, length):
        total = np.dot(Lambda, x_all[:, i-1: i]) + np.dot(B_sot, step * u[i: i+1, 0:1])
        for j in range(1, i+1):
            total += np.dot(Theta * cj(b, j) * step ** a, x_all[:, j-1: j])
        x_all[:, i: i+1] = total

    return x_all


print u_list
print true_simulation(time, u_list)[0, :]
# plt.plot(time, true_simulation(u_list)[0, :])
# plt.show()